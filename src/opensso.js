'use strict'

class OpenSSO {
  constructor (opt) {
    if (typeof opt === 'object' && opt.constructor === Object) {
      this.protected_path = []
      this.selector_by = 'attribute'
      this.selector_attribute_name = 'data-opensso'
      this.selector_prefix = 'opensso-'
      for (const key in opt) {
        if (Object.prototype.hasOwnProperty.call(opt, 'sso_url')) {
          this[key] = opt[key]
        }
      }
      if (!opt.base_url) {
        throw new Error('Option base_url is required!')
      }
      if (!opt.sso_url) {
        throw new Error('Option sso_url is required!')
      } else {
        this.key = this.sso_url.split('/sso/login/')[1]
      }
    } else {
      throw new Error('Option must be an object type!')
    }
    return this
  }

  _getElementLink (section) {
    section = section.toLowerCase()
    switch (this.selector_by) {
      case 'attribute':
        return document.querySelector('[' + this.selector_attribute_name + '="' + this.selector_prefix + section + '"]')
      case 'class':
        return document.getElementsByClassName(this.selector_prefix + section)[0]
      case 'id':
        return document.getElementById(this.selector_prefix + section)
    }
  }

  _injectorLink (section) {
    let el = null
    section = section.toLowerCase()
    switch (section) {
      case 'login':
        el = this._getElementLink(section)
        if (el) {
          el.setAttribute('onclick', `location.href = '${this.sso_url}'`)
        }
        break
      case 'logout':
        el = this._getElementLink(section)
        if (el) {
          el.setAttribute('onclick', `location.href = '${this.base_url + '?logout=' + this.key}'`)
        }
        break
    }
  }

  _refresh (_cb) {
    const elLogin = this._getElementLink('login')
    const elLogout = this._getElementLink('logout')
    // validate token
    this.validateToken(function (err, status) {
      if (err) {
        if (_cb && typeof _cb === 'function') {
          return _cb(err, false)
        }
      }
      if (status) {
        // still login
        if (elLogin) elLogin.style.display = 'none'
        if (elLogout) elLogout.setAttribute('style', 'display:block-inline')
        if (_cb && typeof _cb === 'function') {
          return _cb(null, true)
        }
      } else {
        // not login
        if (elLogout) elLogout.style.display = 'none'
        if (elLogin) elLogin.setAttribute('style', 'display:block-inline')
        if (_cb && typeof _cb === 'function') {
          return _cb(null, false)
        }
      }
    })
  }

  _handleProtectedPage () {
    if (this.protected_path.length > 0) {
      // is protected page
      let isProtectedPage = false
      for (let i = 0; i < this.protected_path.length; i++) {
        if (window.location.href.indexOf(this.protected_path[i]) !== -1) {
          isProtectedPage = true
        }
      }
      if (isProtectedPage) {
        const baseUrl = this.base_url
        this.validateToken(function (err, status) {
          if (err) {
            window.location.href = baseUrl
            return
          }
          if (!status) {
            window.location.href = baseUrl
          }
        })
      }
    }
  }

  _removeParam (url, parameter) {
    const urlparts = url.split('?')
    if (urlparts.length >= 2) {
      const prefix = encodeURIComponent(parameter) + '='
      const pars = urlparts[1].split(/[&;]/g)
      // reverse iteration as may be destructive
      for (let i = pars.length; i-- > 0;) {
        // idiom for string.startsWith
        if (pars[i].lastIndexOf(prefix, 0) !== -1) {
          pars.splice(i, 1)
        }
      }
      url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '')
      return url
    } else {
      return url
    }
  }

  removeUrlParam (paramName) {
    // update parameter on url
    const nurl = this._removeParam(window.location.href, paramName)
    // set replace parameter
    window.history.replaceState({}, document.title, nurl)
  }

  getUrlParam (url) {
    if (url) {
      const query = url.split('?')
      if (query[1]) {
        const data = {}
        const queries = query[1].split('&')
        for (let i = 0; i < queries.length; i++) {
          const item = queries[i].split('=')
          data[item[0]] = decodeURIComponent(item[1])
        }
        return data
      }
    }
    return ''
  }

  parseJWT (_token) {
    const base64Url = _token.split('.')[1]
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
    return JSON.parse(jsonPayload)
  }

  saveToken (_token) {
    // Extract data token
    const data = this.parseJWT(_token)

    // Generate time
    const dt = new Date()
    const getTime = dt.getTime()
    const result = {
      token: _token,
      generate: getTime,
      expire: (data.exp * 1000)
    }

    // Saving to localstorage
    try {
      window.localStorage.setItem(this.key, JSON.stringify(result))
      return true
    } catch (err) {
      console.log(err)
      return false
    }
  }

  validateToken (_cb) {
    try {
      if (window.localStorage.getItem(this.key)) {
        const openssoData = window.localStorage.getItem(this.key)
        if (new Date().getTime() > openssoData.expire) {
          window.localStorage.removeItem(this.key)
          if (_cb && typeof _cb === 'function') {
            return _cb(null, false)
          }
        }
        if (_cb && typeof _cb === 'function') {
          return _cb(null, true)
        }
      } else {
        if (_cb && typeof _cb === 'function') {
          return _cb('Token not available!!!', null)
        }
      }
    } catch (err) {
      if (_cb && typeof _cb === 'function') {
        return _cb(err, null)
      }
    }
  }

  getToken (_cb) {
    try {
      if (window.localStorage.getItem(this.key)) {
        if (_cb && typeof _cb === 'function') {
          return _cb(null, JSON.parse(window.localStorage.getItem(this.key)).token)
        }
      }
    } catch (err) {
      if (_cb && typeof _cb === 'function') {
        return _cb(err, null)
      }
    }
  }

  clearToken () {
    if (window.localStorage.getItem(this.key)) {
      window.localStorage.removeItem(this.key)
    }
  }

  automation () {
    // inject button
    this._injectorLink('login')
    this._injectorLink('logout')

    // handle token
    const urlQuery = this.getUrlParam(window.location.href)
    const token = urlQuery.token
    if (token) {
      // parseJWT
      try {
        this.parseJWT(token)
        if (this.saveToken(token)) {
          this._refresh()
          this.removeUrlParam('token')
        }
      } catch (err) {
        // do nothing
      }
    }

    // handle logout
    const logout = urlQuery.logout
    if (logout) {
      this.clearToken()
      this._refresh()
      this.removeUrlParam('logout')
    }

    // handle protected page
    this._handleProtectedPage()

    // validate
    this._refresh()
  }
}

module.exports = OpenSSO
