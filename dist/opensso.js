"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
(function (f) {
  if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object" && typeof module !== "undefined") {
    module.exports = f();
  } else if (typeof define === "function" && define.amd) {
    define([], f);
  } else {
    var g;
    if (typeof window !== "undefined") {
      g = window;
    } else if (typeof global !== "undefined") {
      g = global;
    } else if (typeof self !== "undefined") {
      g = self;
    } else {
      g = this;
    }
    g.OpenSSO = f();
  }
})(function () {
  var define, module, exports;
  return function () {
    function r(e, n, t) {
      function o(i, f) {
        if (!n[i]) {
          if (!e[i]) {
            var c = "function" == typeof require && require;
            if (!f && c) return c(i, !0);
            if (u) return u(i, !0);
            var a = new Error("Cannot find module '" + i + "'");
            throw a.code = "MODULE_NOT_FOUND", a;
          }
          var p = n[i] = {
            exports: {}
          };
          e[i][0].call(p.exports, function (r) {
            var n = e[i][1][r];
            return o(n || r);
          }, p, p.exports, r, e, n, t);
        }
        return n[i].exports;
      }
      for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) o(t[i]);
      return o;
    }
    return r;
  }()({
    1: [function (require, module, exports) {
      'use strict';

      var OpenSSO = /*#__PURE__*/function () {
        function OpenSSO(opt) {
          _classCallCheck(this, OpenSSO);
          if (_typeof(opt) === 'object' && opt.constructor === Object) {
            this.protected_path = [];
            this.selector_by = 'attribute';
            this.selector_attribute_name = 'data-opensso';
            this.selector_prefix = 'opensso-';
            for (var key in opt) {
              if (Object.prototype.hasOwnProperty.call(opt, 'sso_url')) {
                this[key] = opt[key];
              }
            }
            if (!opt.base_url) {
              throw new Error('Option base_url is required!');
            }
            if (!opt.sso_url) {
              throw new Error('Option sso_url is required!');
            } else {
              this.key = this.sso_url.split('/sso/login/')[1];
            }
          } else {
            throw new Error('Option must be an object type!');
          }
          return this;
        }
        _createClass(OpenSSO, [{
          key: "_getElementLink",
          value: function _getElementLink(section) {
            section = section.toLowerCase();
            switch (this.selector_by) {
              case 'attribute':
                return document.querySelector('[' + this.selector_attribute_name + '="' + this.selector_prefix + section + '"]');
              case 'class':
                return document.getElementsByClassName(this.selector_prefix + section)[0];
              case 'id':
                return document.getElementById(this.selector_prefix + section);
            }
          }
        }, {
          key: "_injectorLink",
          value: function _injectorLink(section) {
            var el = null;
            section = section.toLowerCase();
            switch (section) {
              case 'login':
                el = this._getElementLink(section);
                if (el) {
                  el.setAttribute('onclick', "location.href = '".concat(this.sso_url, "'"));
                }
                break;
              case 'logout':
                el = this._getElementLink(section);
                if (el) {
                  el.setAttribute('onclick', "location.href = '".concat(this.base_url + '?logout=' + this.key, "'"));
                }
                break;
            }
          }
        }, {
          key: "_refresh",
          value: function _refresh(_cb) {
            var elLogin = this._getElementLink('login');
            var elLogout = this._getElementLink('logout');
            // validate token
            this.validateToken(function (err, status) {
              if (err) {
                if (_cb && typeof _cb === 'function') {
                  return _cb(err, false);
                }
              }
              if (status) {
                // still login
                if (elLogin) elLogin.style.display = 'none';
                if (elLogout) elLogout.setAttribute('style', 'display:block-inline');
                if (_cb && typeof _cb === 'function') {
                  return _cb(null, true);
                }
              } else {
                // not login
                if (elLogout) elLogout.style.display = 'none';
                if (elLogin) elLogin.setAttribute('style', 'display:block-inline');
                if (_cb && typeof _cb === 'function') {
                  return _cb(null, false);
                }
              }
            });
          }
        }, {
          key: "_handleProtectedPage",
          value: function _handleProtectedPage() {
            if (this.protected_path.length > 0) {
              // is protected page
              var isProtectedPage = false;
              for (var i = 0; i < this.protected_path.length; i++) {
                if (window.location.href.indexOf(this.protected_path[i]) !== -1) {
                  isProtectedPage = true;
                }
              }
              if (isProtectedPage) {
                var baseUrl = this.base_url;
                this.validateToken(function (err, status) {
                  if (err) {
                    window.location.href = baseUrl;
                    return;
                  }
                  if (!status) {
                    window.location.href = baseUrl;
                  }
                });
              }
            }
          }
        }, {
          key: "_removeParam",
          value: function _removeParam(url, parameter) {
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {
              var prefix = encodeURIComponent(parameter) + '=';
              var pars = urlparts[1].split(/[&;]/g);
              // reverse iteration as may be destructive
              for (var i = pars.length; i-- > 0;) {
                // idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                  pars.splice(i, 1);
                }
              }
              url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
              return url;
            } else {
              return url;
            }
          }
        }, {
          key: "removeUrlParam",
          value: function removeUrlParam(paramName) {
            // update parameter on url
            var nurl = this._removeParam(window.location.href, paramName);
            // set replace parameter
            window.history.replaceState({}, document.title, nurl);
          }
        }, {
          key: "getUrlParam",
          value: function getUrlParam(url) {
            if (url) {
              var query = url.split('?');
              if (query[1]) {
                var data = {};
                var queries = query[1].split('&');
                for (var i = 0; i < queries.length; i++) {
                  var item = queries[i].split('=');
                  data[item[0]] = decodeURIComponent(item[1]);
                }
                return data;
              }
            }
            return '';
          }
        }, {
          key: "parseJWT",
          value: function parseJWT(_token) {
            var base64Url = _token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
              return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
            return JSON.parse(jsonPayload);
          }
        }, {
          key: "saveToken",
          value: function saveToken(_token) {
            // Extract data token
            var data = this.parseJWT(_token);

            // Generate time
            var dt = new Date();
            var getTime = dt.getTime();
            var result = {
              token: _token,
              generate: getTime,
              expire: data.exp * 1000
            };

            // Saving to localstorage
            try {
              window.localStorage.setItem(this.key, JSON.stringify(result));
              return true;
            } catch (err) {
              console.log(err);
              return false;
            }
          }
        }, {
          key: "validateToken",
          value: function validateToken(_cb) {
            try {
              if (window.localStorage.getItem(this.key)) {
                var openssoData = window.localStorage.getItem(this.key);
                if (new Date().getTime() > openssoData.expire) {
                  window.localStorage.removeItem(this.key);
                  if (_cb && typeof _cb === 'function') {
                    return _cb(null, false);
                  }
                }
                if (_cb && typeof _cb === 'function') {
                  return _cb(null, true);
                }
              } else {
                if (_cb && typeof _cb === 'function') {
                  return _cb('Token not available!!!', null);
                }
              }
            } catch (err) {
              if (_cb && typeof _cb === 'function') {
                return _cb(err, null);
              }
            }
          }
        }, {
          key: "getToken",
          value: function getToken(_cb) {
            try {
              if (window.localStorage.getItem(this.key)) {
                if (_cb && typeof _cb === 'function') {
                  return _cb(null, JSON.parse(window.localStorage.getItem(this.key)).token);
                }
              }
            } catch (err) {
              if (_cb && typeof _cb === 'function') {
                return _cb(err, null);
              }
            }
          }
        }, {
          key: "clearToken",
          value: function clearToken() {
            if (window.localStorage.getItem(this.key)) {
              window.localStorage.removeItem(this.key);
            }
          }
        }, {
          key: "automation",
          value: function automation() {
            // inject button
            this._injectorLink('login');
            this._injectorLink('logout');

            // handle token
            var urlQuery = this.getUrlParam(window.location.href);
            var token = urlQuery.token;
            if (token) {
              // parseJWT
              try {
                this.parseJWT(token);
                if (this.saveToken(token)) {
                  this._refresh();
                  this.removeUrlParam('token');
                }
              } catch (err) {
                // do nothing
              }
            }

            // handle logout
            var logout = urlQuery.logout;
            if (logout) {
              this.clearToken();
              this._refresh();
              this.removeUrlParam('logout');
            }

            // handle protected page
            this._handleProtectedPage();

            // validate
            this._refresh();
          }
        }]);
        return OpenSSO;
      }();
      module.exports = OpenSSO;
    }, {}]
  }, {}, [1])(1);
});
