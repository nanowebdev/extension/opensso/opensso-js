/* global describe it */
const assert = require('assert')
const OpenSSO = require('../src/opensso.js')

describe('Basic Test', () => {
  it('constructor', () => {
    // no options will caught error
    assert.throws(() => {
      const opensso = new OpenSSO()
      console.log(opensso)
    }, Error)

    // no sso_url option will caught error
    assert.throws(() => {
      const opensso = new OpenSSO({
        base_url: './',
        protected_path: []
      })
      console.log(opensso)
    }, Error)

    // no base_url option will caught error
    assert.throws(() => {
      const opensso = new OpenSSO({
        sso_url: 'tester'
      })
      console.log(opensso)
    }, Error)

    const ssoKey = '53ae91f41f8544c29b30a2092dd3f8f5'
    const ssoUrl = 'http://localhost:3000/sso/login/' + ssoKey
    const opensso = new OpenSSO({
      base_url: './',
      sso_url: ssoUrl,
      custom_key: 'abc'
    })
    assert.strictEqual(opensso.sso_url, ssoUrl)
    assert.strictEqual(opensso.key, ssoKey)
    assert.strictEqual(opensso.custom_key, 'abc')
    assert.deepStrictEqual(opensso.protected_path, [])
  })

  it('Parse JWT', () => {
    const token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJlZDkwYjkyNC1lNjg0LTQyZWItOTkwMy1iMzlhNjMzNTg3ZTEiLCJ1bm0iOiJhYWxmaWFubiIsIm5hbWUiOiJhYWxmaWFubiIsIm1haWwiOiJhYWxmaWFubkBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJncmF2YXRhciI6Imh0dHBzOi8vZ3JhdmF0YXIuY29tL2F2YXRhci82ZDdiZDI0MWEyNjc5ZDc1NTc4OTQwMjY4MWQyNWVjMSIsImhhc2giOiJFNDBNRGJqb0cxcHBoWndNak9UVSIsImlhdCI6MTY5NjE1OTA3MCwiZXhwIjoxNjk2MTg3ODcwfQ.U_cnuKkVUm0vkgIEqs6KmyInbSlkvmRuXxuwZrJOCv2TIbEoKKc3pV1YA1uuhTKq2O56ABWU_xdRQ_Xz9LphTpmQk7DlbK1OGJaxjDXrdeKrUTYoyZ_reYWucvGs8RTRMBdxLQFOpApZ9q-QsSiNdFLqjVSHaLll9WQcBgnD6n3TyF6Mnu4Jx7RpRgCw4mlcnTOsmZStw1w2k6jpdDWDPvx4Qr6oqJ_6RQm7fV-6FwIh8XFCKveFZ8nf4xcYwqVGs3roElpKqmrzggATmdo8TnF3WPPbYeGMB2y2uYY3jFi5QRYH-t16fCzBL-t0hja8CGkd2qGph9msoCHGd3NWlg'
    const ssoKey = '53ae91f41f8544c29b30a2092dd3f8f5'
    const ssoUrl = 'http://localhost:3000/sso/login/' + ssoKey
    const opensso = new OpenSSO({
      base_url: './',
      sso_url: ssoUrl
    })
    assert.strictEqual(opensso.parseJWT(token).uid, 'ed90b924-e684-42eb-9903-b39a633587e1')
  })

  it('Get and Remove URL Parameter', () => {
    const url = 'http://localhost/static?token=abc'
    const ssoKey = '53ae91f41f8544c29b30a2092dd3f8f5'
    const ssoUrl = 'http://localhost:3000/sso/login/' + ssoKey
    const opensso = new OpenSSO({
      base_url: './',
      sso_url: ssoUrl
    })
    assert.deepStrictEqual(opensso.getUrlParam(url), { token: 'abc' })
    assert.strictEqual(opensso._removeParam(url, 'token'), 'http://localhost/static')
  })
})
